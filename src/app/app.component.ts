import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Product } from './shared/model';
import { ProductService } from './shared/product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  products: any;
  addProductForm: FormGroup;
  updateProductForm: FormGroup;
  showAddForm = false;
  showUpdateForm = false;

  constructor(private productService: ProductService, private fb: FormBuilder) {
    this.addProductForm = this.fb.group({
      productName: ['', Validators.required],
      quantity: 0,
      price: 0,
      promotionPrice: 0,
      image: ['', Validators.required],
    });
    this.updateProductForm = this.fb.group({
      id:'',
      productName: '',
      quantity: 0,
      price: 0,
      promotionPrice: 0,
      image: '',
    });
  }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts() {
    this.productService.getProducts().subscribe(res => {
      if (res.status == 200) {
        this.products = res.body;
      }
      else {
        alert('Get data failed');
      }
    });
  }

  deleteProduct(id: string) {
    this.productService.deleteProduct(id).subscribe(res => {
      if (res.status == 200) {
        alert('Delete success');
        this.getProducts();
      }
      else {
        alert('Delete failed');
      }
    })
  }

  updateProduct() {
    this.productService.updateProduct(this.updateProductForm.value).subscribe(res => {
      if (res.status == 200) {
        alert('Update success');
        this.getProducts();
      }
      else {
        alert('Update failed');
      }
    })
  }

  getUpdateForm(item: Product) {
    this.showUpdateForm = true;
    this.updateProductForm = this.fb.group({
      id: item.id,
      productName: [item.productName, Validators.required],
      quantity: item.quantity,
      price: item.price,
      promotionPrice: item.promotionPrice,
      image: [item.image, Validators.required],
    });
  }

  addProduct() {
    this.productService.addProduct(this.addProductForm.value).subscribe(res => {
      if (res.status == 200) {
        alert('Add product success');
        this.getProducts();
      }
      else {
        alert('Add product failed');
      }
    })
  }
}
