import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from './model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpClient: HttpClient) { }

  // getProducts() {
  //   const apiUrl='https://steelsoftware.azurewebsites.net/api/FresherFPT';
  //   return this.httpClient.get(apiUrl);
  // }

  getProducts(): Observable<HttpResponse<Product[]>> {
    const apiUrl = 'https://steelsoftware.azurewebsites.net/api/FresherFPT';
    const options = {
      headers: new HttpHeaders({
        'Content-type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<Product[]>(apiUrl, options);
  }

  deleteProduct(id: string) {
    const apiUrl = `https://steelsoftware.azurewebsites.net/api/FresherFPT/${id}`;
    return this.httpClient.delete(apiUrl, { observe: 'response' });
  }

  updateProduct(product: any) {
    const apiUrl = 'https://steelsoftware.azurewebsites.net/api/FresherFPT/';
    const options = {
      headers: new HttpHeaders({
        'Content-type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.put(apiUrl, product, options);
  }

  addProduct(formData:any){
    const apiUrl = 'https://steelsoftware.azurewebsites.net/api/FresherFPT/';
    const data = JSON.stringify(formData);
    const options = {
      headers: new HttpHeaders({
        'Content-type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.post(apiUrl, data, options);
  }
}
